# About Me 🛠️

I am a backend engineer at Gitlab, proficient in Ruby and currently expanding my skills in Rust and Go.

## Working Style 💼

- **Async and Sync Collaboration:** I thrive in asynchronous work environments, but I also relish sync pair programming on new challenges.
- **Coffee Chats:** I enjoy occasional coffee chats; feel free to schedule one anytime ☕.
- **Scalability Focus:** I lean towards scalable solutions, emphasizing the importance of a scalable codebase design while avoiding over-engineering.
- **Conventional Comments:** I follow the [Conventional Comments](https://conventionalcomments.org/#format) format and appreciate others using it.
- **Iteration and Small Scope:** I advocate for an iterative approach with many small merge requests, prioritizing error-free changes.
- **Manager of One:** I dislike micro-management but ensure self-discipline to stay on track.
- **Async Updates and Personal OKRs:** I use asynchronous updates and treat my development and growth as my personal OKRs.
- **TODOs and Structured App:** I heavily rely on TODOs; alongside Gitlab Todos, I use [Structured App](https://structured.app/) for organization.
- **Attention Requests:** Mention me for anything that needs my attention promptly.

### Work Hours 🕒

- **Routine Work Hours:** I maintain a 9:00 to 17:30 CET work style for a healthy work-life balance.
- **Flexible Hours:** Occasionally, I adapt to non-linear working hours as project demands dictate.
- **Code Review Time:** I dedicate a specific time, usually 13:00 CET, for code reviews. MRs assigned before that time will be quickly reviewed.

## Personal Insights 🌟

### Daily Routine 🌅

- **Self-Discipline:** I wake up at 5:30 AM, engage in sports, have breakfast, allocate time for personal projects, focus on work, hit the gym, and cherish personal time.
- **Travel Enthusiast:** I love traveling; feel free to reach out for tips or if you're seeking a travel companion.

### Hobbies 🎉

- **Soccer Enthusiast:** A football fanatic, enjoying the beautiful game.
- **History and Politics:** Engaging in discussions on history and politics outside of work hours.
- **Adventure Seeker:** Exploring local experiences on trips and embracing new adventures.

Feel free to connect for any work-related collaborations or just to chat about shared interests! 🚀
