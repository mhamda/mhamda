# About Me 🛠️

- **Role:** Backend engineer at Gitlab.
- **Skills:** Proficient in Ruby, expanding into Rust and Go.
- **Work Environments:** Thrives in both asynchronous and synchronous settings.
- **Communication:** Enjoys collaborative coffee chats.
- **Coding Philosophy:** Emphasizes scalable solutions without unnecessary complexity.
- **Work Style:** Iterative approach, autonomy, and structured task management.
  
# Personal Insights 🌟

- **Routine:** Maintains a disciplined daily routine.
- **Interests:** Enjoys travel, soccer, history, and politics.

# Uncover Me 🥷

Explore the nuances of my work style and personal insights in [my detailed README](https://gitlab.com/mhamda/mhamda/-/blob/main/detailed.md).
